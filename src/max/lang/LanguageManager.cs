﻿using max.IO;
using max.serialize;
using max.util;
using System.Collections.Generic;

namespace max.lang
{
    /// <summary>
    /// A class that manages language strings.
    /// </summary>
    public static class LanguageManager
    {
        /// <summary>
        /// The directory in the game's AppData folder where the
        /// language files are stored.
        /// </summary>
        /// <value>Directory name</value>
        public static string ConfigDirectory { get; set; } = "lang";

        /// <summary>
        /// The language to use. Directory names are standardized to be 2 character abbreviations.
        /// </summary>
        /// <value>Language directory name</value>
        public static string Language { get; set; } = "en";

        /// <summary>
        /// Returns the currently used language directory. Includes the base directory
        /// and the language directory.
        /// </summary>
        /// <value>Language directory name</value>
        public static string LanguageDirectory { get { return "\\" + ConfigDirectory + "\\" + Language; } }

        /// <summary>
        /// The file extension of the input controller data.
        /// </summary>
        /// <value>File extension (including period)</value>
        public static string ConfigExt { get; set; } = ".lang";

        /// <summary>
        /// The directory in AppData where the configs are saved. The default
        /// is "MaxLib", so cross-platform standards could be developed that could
        /// be used with a wide variety of games. If your config has a custom
        /// control scheme, it should have its own directory.
        /// </summary>
        /// <value>Directory name</value>
        public static string AppDirectoryName { get; set; } = "MaxLib";

        //holds the language data.
        static Dictionary<string, Dictionary<string, string>> Lang = new Dictionary<string, Dictionary<string, string>>();

        /// <summary>
        /// Adds a language group from a file.
        /// </summary>
        /// <param name="LangGroupID">Language group ID</param>
        /// <param name="Filename">Language file</param>
        public static void Load(string LangGroupID, string Filename)
        {
            // gets the data from the file. its stored in YAML as a set of key pairs
            MaxSerializedObject Data = MaxSerializer.DeserializeYAML(TextFileReader.ReadFile(LanguageDirectory, Filename, ConfigExt));
            if (Data != null)
            {
                // turns the data into a Dictionary<string,string>
                Dictionary<string, string> LangData = new Dictionary<string, string>();

                foreach (KeyValuePair<string, object> d in Data.Data)
                {
                    LangData.Add(d.Key, (string)d.Value);
                }

                // if the lang data already exists
                if (Lang.ContainsKey(LangGroupID))
                {
                    // replace it
                    Lang[LangGroupID] = LangData;
                }
                else
                {
                    // otherwise add it
                    Lang.Add(LangGroupID, LangData);
                }
            }
        }

        /// <summary>
        /// Adds a language group from a file.
        /// </summary>
        /// <param name="LangGroupID">Language group ID</param>
        /// <param name="Directory">Directory of the language file</param>
        /// <param name="Filename">Language file</param>
        public static void Load(string LangGroupID, string Directory, string Filename)
        {
            Load(LangGroupID,Directory + "\\"+ Filename);
        }

        /// <summary>
        /// Unloads a language group from memory.
        /// </summary>
        /// <param name="LangGroupID">Language group ID</param>
        public static void Unload(string LangGroupID)
        {
            Lang.Remove(LangGroupID);
        }

        /// <summary>
        /// Gets a language string and replaces the variables in the string.
        /// </summary>
        /// <param name="LangGroupID">Language group ID</param>
        /// <param name="LangID">Language string key</param>
        /// <returns>String</returns>
        public static string Get(string LangGroupID, string LangID)
        {
            if (Lang.ContainsKey(LangGroupID))
            {
                if (Lang[LangGroupID].ContainsKey(LangID))
                {
                    return Lang[LangGroupID][LangID];
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a language string and replaces the variables in the string.
        /// </summary>
        /// <param name="LangGroupID">Language group ID</param>
        /// <param name="LangID">Language string key</param>
        /// <param name="Variables">Variables to replace</param>
        /// <returns>String</returns>
        public static string Get(string LangGroupID, string LangID, string[] Variables)
        {
            return StringUtil.ReplaceVariables(Get(LangGroupID, LangID), Variables);
        }
    }
}
