# Using max.lang

``max.lang`` is a MaxLib Helper Library that provides multi-language support.

* [Installation](Installation.md)
* [Using the Language Manager](LangManager.md)

## Max Dependencies:

* [max.io](https://labadore64.gitlab.io/max.io/)
* [max.serialize](https://labadore64.gitlab.io/max.serialize/)
* [max.util](https://labadore64.gitlab.io/max.util/)