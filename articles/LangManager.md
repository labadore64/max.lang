# Using the Language Manager

The language manager allows you to easily manage language strings through external files that are loaded and unloaded at runtime.

## File Structure

Files store language strings in a list of key-pairs for each line like this: ``[key]:[value]``.

You should surround your strings with quotation marks to ensure the entire string is parsed.

```
test_string1: "hello world"
test_string2: "my final message"
test_string3: "goodbye"
test_string4: "0x&1 birds flying"
```

You can also use plaintext files as well with no structure.

### File System

Language files by default use the ``.lang`` file extension. You can customize the extension by setting ``LanguageManager.ConfigExt``.

```
// Change the language files extension to txt
LanguageManager.ConfigExt = ".txt";
```

By default, the language directory is set to ``lang``, but it can be set to any directory in the app directory.

```
// Change the language files extension to language
LanguageManager.ConfigDirectory = "language";
```

On the filesystem, you should structure the directories in the following way so that the Language Manager can read them.

```
- [App Directory]
  - [language directory]
    - en
      - [English language files start here]
    - de
      - [German language files start here]
    ...
```

## Usage

### Setting the Language

To set the current language, set ``LanguageManager.Language``.

```
// Change the language to "de"
LanguageManager.Language = "de";
```

If you change the language, currently loaded language files will not be unloaded. You must reload these language files.

### Loading Language Files

To load data from a language file, call ``Load()`` with the ID of the language data and the file it extracts from relative to the language path. All language data from the file will be loaded into the language data in this ID.

```
// loads language ID "test_lang"
LanguageManager.Load("test_lang", "testlang");
```

This will load the data from the file from ``%APPDATA%\[AppDirectory]\[ConfigDirectory]\[Language]\testlang.lang``.

### Unloading Language Data

To unload language data, call ``Unload()`` with the ID of the language data to unload. All language data in that ID will be unloaded.

```
// unloads language ID "test_lang"
LanguageManager.Unload("test_lang");
```

### Reading Language Data

To retrieve data from the Language Manager, call ``Get()`` with the ID of the language group you're looking for and the ID of the specific string you're looking for.

```
// Prints "hello world"
Console.WriteLine(LanguageManager.Get("test_lang","test_string1"));

// Prints "goodbye"
Console.WriteLine(LanguageManager.Get("test_lang","test_string3"));
```

You can also replace variables based the prefix defined in ``StringUtil.ReplacePrefix`` by passing the variables as a string array.

```
// Prints "33 birds flying"
Console.WriteLine(LanguageManager.Get(
	"test_lang",
	"test_string4",
	new string[] 
	{ "33" }
));
```