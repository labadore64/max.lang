# max.lang

``max.lang`` is a MaxLib Helper Library that provides multi-language support.

* [Documentation](https://labadore64.gitlab.io/max.lang/)
* [Source](https://gitlab.com/labadore64/max.lang/)